import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
// import 'package:scoped_model/scoped_model.dart';

// import '../backend/models.dart';

class SignUpPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _passwordTextController = TextEditingController();
  // static DateFormat formatter = new DateFormat('dd-MM-yyyy');
  String dobText = "Click here to Select Date of Birth";
  final Map<String, dynamic> _formData = {
    'name': null,
    'email': null,
    'password': null,
    'mobile': null,
    'dob': null
  };

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SignupPage(context);
  }

  Widget nameTextField() {
    return Container(
      margin: EdgeInsets.only(left:15,right:15),
      padding: EdgeInsets.only(left: 25.0, right: 25.0),
      child: TextFormField(
        keyboardType: TextInputType.text,
        textAlign: TextAlign.left,
        decoration: InputDecoration(
          fillColor: Colors.white,
          labelText: 'Enter your Name',
          labelStyle: TextStyle(color: Colors.red, fontSize: 16.0,fontWeight: FontWeight.bold),
          hintStyle: TextStyle(color: Colors.grey),
          hintText: 'xxxxx xxxxx',
        ),
        style: TextStyle(
          color: Colors.white
        ),
        validator: (String value) {
          if (value.isEmpty || value.length < 5) {
            return 'Please enter a valid Name with atleast 5 character';
          }
        },
        onSaved: (String value) {
          _formData['name'] = value;
        },
      ),
    );
  }

  Widget emailTextField() {
    return Container(
      margin: EdgeInsets.only(left:15,right:15),
      padding: EdgeInsets.only(left: 25.0, right: 25.0),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        textAlign: TextAlign.left,
        decoration: InputDecoration(
          fillColor: Colors.white,
          labelText: 'Enter your Email',
          labelStyle: TextStyle(color: Colors.red, fontSize: 16.0,fontWeight: FontWeight.bold),
          hintStyle: TextStyle(color: Colors.grey),
          hintText: 'xxx@xxx.xxx',
        ),
        style: TextStyle(
          color: Colors.white
        ),
        validator: (String value) {
          if (value.isEmpty ||
              !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                  .hasMatch(value)) {
            return 'Please enter a valid email';
          }
        },
        onSaved: (String value) {
          _formData['email'] = value;
        },
      ),
    );
  }

  Widget mobileTextField() {
    return Container(
      margin: EdgeInsets.only(left:15,right:15),
      padding: EdgeInsets.only(left: 25.0, right: 25.0),
      child: TextFormField(
        keyboardType: TextInputType.number,
        textAlign: TextAlign.left,
        decoration: InputDecoration(
          fillColor: Colors.white,
          labelText: 'Enter your Mobile No.',
          labelStyle: TextStyle(color: Colors.red, fontSize: 16.0,fontWeight: FontWeight.bold),
          hintStyle: TextStyle(color: Colors.grey),
          hintText: '01XXXXXXXXX',
        ),
        style: TextStyle(
          color: Colors.white
        ),
        validator: (String value) {
          if (value.isEmpty || !RegExp(r"[0][1-9][0-9]{9}").hasMatch(value)) {
            return 'Please enter a valid mobile number';
          }
        },
        onSaved: (String value) {
          _formData['mobile'] = value;
        },
      ),
    );
  }

  Widget passTextField() {
    return Container(
      margin: EdgeInsets.only(left:15,right:15),
      padding: EdgeInsets.only(left: 25.0, right: 25.0),
      child: TextFormField(
        controller: _passwordTextController,
        keyboardType: TextInputType.text,
        obscureText: true,
        textAlign: TextAlign.left,
        decoration: InputDecoration(
          labelText: 'Enter your Password',
          labelStyle: TextStyle(color: Colors.red, fontSize: 16.0,fontWeight: FontWeight.bold),
          hintStyle: TextStyle(color: Colors.grey),
          hintText: 'XXXXXXXXX',
        ),
        style: TextStyle(
          color: Colors.white
        ),
        validator: (String value) {
          if (value.isEmpty || value.length < 6) {
            return 'Please enter a atleast 6 character password';
          }
        },
        onSaved: (String value) {
          _formData['password'] = value;
        },
      ),
    );
  }

  Widget passConformTextField() {
    return Container(
      margin: EdgeInsets.only(left:15,right:15),
      padding: EdgeInsets.only(left: 25.0, right: 25.0),
      child: TextFormField(
        keyboardType: TextInputType.text,
        obscureText: true,
        textAlign: TextAlign.left,
        decoration: InputDecoration(
          labelText: 'Conform your Password',
          labelStyle: TextStyle(color: Colors.red, fontSize: 16.0,fontWeight: FontWeight.bold),
          hintStyle: TextStyle(color: Colors.grey),
          hintText: 'XXXXXXXXX',
        ),
        style: TextStyle(
          color: Colors.white
        ),
        validator: (String value) {
          if (_passwordTextController.text != value) {
            return 'Password didn\'t match';
          }
        },
      ),
    );
  }

  Future _submitForm(Function register) async {
    if (!_formKey.currentState.validate() && _formData['dob'] == null) {
      return;
    }
    _formKey.currentState.save();
    Map<String, dynamic> value;
    value = await register(
        _formData['name'].toString(),
        _formData['email'].toString(),
        _formData['mobile'].toString(),
        _formData['password'].toString());
    if (value['success']) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Registration Done'),
            content: Text('Please login now.'),
            actions: <Widget>[
              FlatButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.pushNamed(context, '/login');
                },
              )
            ],
          );
        },
      );
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('An Error Occurred!'),
            content: Text(value['message']),
            actions: <Widget>[
              FlatButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    }
  }

  Widget SignupPage(BuildContext context) {
    return Scaffold(
      body: Stack(fit: StackFit.expand, children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.teal[50],
          ),
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 55.0,
              ),
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 18),
                      child: Image.asset(
                        'assets/icons/easymeds1.png',
                        height: 130,
                      )
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Text(
                      'Shop',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 32.0,
                        fontWeight: FontWeight.w800,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      'EasyMeds',
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: 32.0,
                        fontWeight: FontWeight.w800,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 40.0,
              ),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    nameTextField(),
                    SizedBox(
                      height: 10.0,
                    ),
                    emailTextField(),
                    SizedBox(
                      height: 10.0,
                    ),
                    mobileTextField(),
                    SizedBox(
                      height: 10.0,
                    ),
                    passTextField(),
                    SizedBox(
                      height: 10.0,
                    ),
                    passConformTextField(),
                    SizedBox(
                      height: 20.0,
                    ),
                    Container(
                      margin: EdgeInsets.only(left:45,right:45),
                      padding: EdgeInsets.all(10.0),
                      child: 
                      // ScopedModelDescendant<Models>(builder:
                      //     (BuildContext context, Widget child, Models model) {
                      //   return model.loading
                      //       ? CircularProgressIndicator(
                      //         valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
                      //       )
                      //       : 
                            FlatButton(
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                              color: Colors.teal,
                              onPressed: () => Navigator.pushNamed(context, '/login'),//_submitForm(model.registration), 
                              child: Container(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 20.0,
                                  horizontal: 20.0,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                        "SIGN UP",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )//;
                      //}),
                    ),
                    SizedBox(
                      height: 25.0,
                    ),
                    Container(
                        padding: EdgeInsets.all(10.0),
                        margin: EdgeInsets.only(left: 100.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Have account?",
                              style: TextStyle(
                                color: Colors.teal,
                                fontSize: 14.0,
                              ),
                            ),
                            FlatButton(
                              onPressed: () => Navigator.pushReplacementNamed(
                                  context, '/login'),
                              child: Text(
                                "Sign In",
                                style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                          ],
                        )),
                  ],
                ),
              ),
            ],
          ),
        )
      ]),
    );
  }
}
