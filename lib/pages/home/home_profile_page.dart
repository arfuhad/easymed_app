import 'package:flutter/material.dart';
import 'package:shopeasymeds/pages/home/elements/home_title_view.dart';

class HomeProfilePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _HomeProfilePage();

}

class _HomeProfilePage extends State<HomeProfilePage> {
  @override
  Widget build(BuildContext context) {
    return profilePage();
  }

  Widget profilePage(){
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(top: 35),
      color:Colors.teal[50],
      child:Column(
        children: <Widget>[
          titleView(first: "Pro", last: "file"),
          profileBanner(),
          profileList(),
          // drugList(),
          // totalBill(),
        ]
      )
    );
  }

  Widget profileBanner(){
    return Container(
      padding: EdgeInsets.only(left: 30, right: 10, top: 5),
      height: 130,
      color: Colors.white,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child:GestureDetector(
              onTap: (){
                //_showDialog(model);
              },
              child: Stack(
                children: <Widget>[
                  Container(
                    height: 95,
                    width: 95,
                    child: CircleAvatar(
                      backgroundImage:  NetworkImage(
                            'https://images.unsplash.com/photo-1566750899397-f27813228625?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80'),
                    ),
                  ),
                ]
              ),
            )
          ),
          Expanded(
            flex: 2,
            child: Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(top: 40, left: 25),
              child: Column(
                children: <Widget>[
                  Text(
                    "User Name",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.redAccent
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "017XXXXXXXXX",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.black
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget profileList(){
    return Container(
      margin: EdgeInsets.only(top: 25),
      height: MediaQuery.of(context).size.height-324,
      child:ListView(
        children: <Widget>[

          profileListTile(),
          profileListTile(),
          profileListTile(),
          profileListTile(),

          SizedBox(height: 15,),
          Divider(),
          SizedBox(height: 15,),

          profileListTile()
        ],
      ),
    );
  }
  
  Widget profileListTile(){
    return Card(
      color: Colors.white,
      child: ListTile(
        title: Text(
          'profile details',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w400
          ),
        ),
        trailing: Icon(
          Icons.arrow_forward_ios,
          color: Colors.black,
        ),
      ),
    );
  }
}