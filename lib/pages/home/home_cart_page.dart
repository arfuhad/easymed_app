import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shopeasymeds/controllers/controller_order.dart';
import 'package:shopeasymeds/models/drug.dart';
import 'package:shopeasymeds/pages/home/elements/home_title_view.dart';

class HomeCartPage extends StatefulWidget{
  OrderControl ocModel;
  HomeCartPage(this.ocModel);
  @override
  State<StatefulWidget> createState() => _HomeCartPage();

}

class _HomeCartPage extends State<HomeCartPage> {
  @override
  Widget build(BuildContext context) {
    return cartPage();
  }

  Widget cartPage(){
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(top: 35),
      color:Colors.teal[50],
      child:Column(
        children: <Widget>[
          titleView(first: "Order ", last: "Cart"),
          drugList(),
          totalBill(),
        ]
      )
    );
  }

  Widget drugList(){
    return Container(
      height: MediaQuery.of(context).size.height - 200,
      child:ScopedModel<OrderControl>(
        model: widget.ocModel,
        child: ScopedModelDescendant<OrderControl>(
          builder: (BuildContext context, Widget child, OrderControl model){
            Widget content = Center(child: Container(child: Text("No item is added"),),);
            try{
              print("object"); 
              if( model.cartDrug.length != 0){//model.cartDrug.data != null ||
                print("have data"); 
                content = ListView.builder(
                  itemBuilder: (context, index) {
                    return drugContainer(model.cartDrug[index]);
                  },
                  itemCount: model.cartDrug.length,
                );
              }
            }catch(e){
              print(e.toString());
            }
            return content;
          },
        ),
      ),
    );
  }

  Widget drugContainer (Drug drug){
    return Card(
      color: Colors.white,
      child:ListTile(
        leading: FadeInImage(
          image: NetworkImage(drug.image),
          fit: BoxFit.contain,
          placeholder: AssetImage('assets/images/default.jpg'),
        ),
        title: Text(
          drug.name,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18
          ),
        ),
        subtitle: Text(
          drug.contain,
          style: TextStyle(
            fontSize: 14,
            color: Colors.black54,
          ),
        ),
        trailing: Column(children: <Widget>[
          SizedBox(height: 5,),
          Text("price : 5*4 = 20\$",style: TextStyle(color: Colors.red),),
          SizedBox(height: 5,),
          Text("Quantity: 4")
        ],),
      )
    );
  }

  Widget totalBill(){
    return Container(
      height: 55,
      color: Colors.grey[350],
      child: Container(
        margin: EdgeInsets.only(left: 25),
        child:Row(
          children: <Widget>[
            Expanded(flex:2,child:Text("Total:", style: TextStyle(fontWeight: FontWeight.bold),)),
            Expanded(flex:1,child:Text("60\$", style: TextStyle(fontWeight: FontWeight.bold,color: Colors.red),)),
            confromButton(),
          ],
        ),
      ),
    );
  }

  Widget confromButton(){
    return Expanded(
      flex:2,
      child: Container(
        margin: EdgeInsets.all(2),
        child: FlatButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0)
          ),
          color: Colors.teal,
          onPressed: () 
              {},//Navigator.pushReplacementNamed(context, '/home');_submitForm(model.authenticate),
          child: Container(
            padding: const EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: 10.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Text(
                    "Conform",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

 