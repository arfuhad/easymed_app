import 'dart:async';

import "package:flutter/material.dart";
import 'package:scoped_model/scoped_model.dart';
import 'package:shopeasymeds/backend/backend_api_response.dart';
import 'package:shopeasymeds/controllers/controller_order.dart';
import 'package:shopeasymeds/models/order.dart';
import 'package:shopeasymeds/pages/elements/slide_image.dart';
import 'package:shopeasymeds/controllers/controller_drug.dart';
import 'package:shopeasymeds/pages/home/elements/product_hori_view.dart';
import 'package:shopeasymeds/pages/home/elements/home_title_view.dart';

class HomeDrugPage extends StatefulWidget {
  // final Models model;this.model

  DrugControl dcModel;
  OrderControl ocModel;

  HomeDrugPage(this.dcModel,this.ocModel);

  @override
  State<StatefulWidget> createState() => _HomeDrugPage();
}

class _HomeDrugPage extends State<HomeDrugPage> {

  Widget content = Container(child: CircularProgressIndicator(),);
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return drugPage();
  }

  Widget drugPage(){
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(top: 35),
      color: Colors.teal[50],
      child: Column(
        children: <Widget>[
          titleView(first: "Shop", last: "EasyMeds"),
          Container(
            height: MediaQuery.of(context).size.height - 144,
            child: ScopedModel<DrugControl>(
              model: widget.dcModel,
              child:  ScopedModelDescendant<DrugControl>(
                  builder: (BuildContext context, Widget child, DrugControl model) {

                    Timer(Duration(seconds: 4), (){
                    });
                    
                    Widget choiceContent = Container();
                    if(model.fetchedDrug.status == Status.COMPLETED){
                      choiceContent = productContainer("Best Choice Products", context, model.fetchedDrug, false, widget.ocModel,model);
                    }else if(model.fetchedDrug.status == Status.ERROR){
                      choiceContent = productContainer("Best Choice Products", context, model.fetchedDrug, true, widget.ocModel,model);
                    }else if(model.fetchedDrug.status == Status.LOADING){
                      choiceContent = Center(
                        child: Container(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }

                    Widget recContent = Container();
                    if(model.recommendedDrug.status == Status.COMPLETED){
                      recContent = productContainer("Best Choice Products", context, model.recommendedDrug, false, widget.ocModel,model);
                    }else if(model.recommendedDrug.status == Status.ERROR){
                      recContent = productContainer("Best Choice Products", context, model.recommendedDrug, true, widget.ocModel,model);
                    }else if(model.recommendedDrug.status == Status.LOADING){
                      recContent = Center(
                        child: Container(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }

                    Widget bestContent = Container();
                    if(model.bestSellingDrug.status == Status.COMPLETED){
                      bestContent = productContainer("Best Selling Products", context, model.bestSellingDrug, false, widget.ocModel,model);
                    }else if(model.bestSellingDrug.status == Status.ERROR){
                      bestContent = productContainer("Best Selling Products", context, model.bestSellingDrug, true, widget.ocModel,model);
                    }else if(model.bestSellingDrug.status == Status.LOADING){
                      bestContent = Center(
                        child: Container(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }
                    
                    return RefreshIndicator(
                      onRefresh: () => model.fetchDrug(),
                      child: Container(
                        child: ListView(
                          children: <Widget>[
                            Container(
                              height: 150,
                              child:ImageCarousel(),
                            ),
                            SizedBox(height: 25,),
                            bestContent,
                            SizedBox(height: 25,),
                            recContent,
                            SizedBox(height: 25,),
                            choiceContent,
                          ],
                        )
                      ),
                    );
                  }
              ),
            ),
          )
        ],
      ),
    );
  }
}