import 'package:flutter/material.dart';

Widget titleView({String first, String last}){
  return Row(
    children: <Widget>[
      Container(
        margin: EdgeInsets.only(left: 25,bottom: 10),
        child:Text(first,
          textAlign: TextAlign.start,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.red,
            fontSize: 35,
          ),
        ),
      ),
      Container(
        margin: EdgeInsets.only(bottom: 10),
        child:Text(last,
          textAlign: TextAlign.start,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 35,
          ),
        ),
      )
    ],
  );
}