import 'package:flutter/material.dart';
import 'package:shopeasymeds/backend/backend_api_response.dart';
import 'package:shopeasymeds/controllers/controller_drug.dart';
import 'package:shopeasymeds/controllers/controller_order.dart';
import 'package:shopeasymeds/models/drug.dart';
import 'package:shopeasymeds/pages/drug_page.dart';

Widget productContainer(String title,BuildContext context, ApiResponse<List<Drug>> drugs, bool value, OrderControl ocModel, DrugControl dcModel, {double fontSize = 16}){
  return Container(
    padding: EdgeInsets.only(left:15,right: 15),
    child: Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Text(
              title,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        Container(
          height: 150,
          child: drugs.data.isEmpty || value ?
          Center(
            child: Container(
              child: Text(
              "Sorry!! Data is not available",
              style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
          :
          ListView.builder(
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return productCard(drugs.data[index],context,dcModel,ocModel);
            },
            itemCount: drugs.data.length,
          ),
        ),
      ],
    ),
  );
}

Widget productCard (Drug drug, BuildContext context,DrugControl dcModel, OrderControl ocModel){
  return GestureDetector(
    onTap: () {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DrugPage(drug,dcModel,ocModel),
        ),
      );
    },
    child: Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      child: Column(
        children: <Widget>[
          Container(
            height: 100,
            child:FadeInImage(
              image: NetworkImage(
                  drug.image),
              fit: BoxFit.contain,
              placeholder: AssetImage('assets/images/default.jpg'),
            ),
          ),
          Container(
            height: 20,
            child:Text(
              drug.name,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 8,right: 8),
            child:Text(
              "Price: ${drug.price}",
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
    )
  );
}