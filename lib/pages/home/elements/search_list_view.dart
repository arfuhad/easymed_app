import 'package:flutter/material.dart';
import 'package:shopeasymeds/backend/backend_api_response.dart';
import 'package:shopeasymeds/controllers/controller_drug.dart';
import 'package:shopeasymeds/controllers/controller_order.dart';
import 'package:shopeasymeds/models/drug.dart';
import 'package:shopeasymeds/pages/drug_page.dart';


Widget drugListViewer (BuildContext context,List<Drug> drugs,DrugControl dcModel,OrderControl ocModel){
  return Container(
    height:MediaQuery.of(context).size.height - 200,
    child:ListView.builder(
      itemBuilder: (context, index) {
        return drugItems(context,drugs[index],dcModel,ocModel);
      },
      itemCount: drugs.length,
    ),
  );
}

double priceGet(String value){
  List<String> list = value.split(" ");
  return double.parse(list[list.length - 2]);
}

Widget drugItems (BuildContext context,Drug drug,DrugControl dcModel,OrderControl ocModel){
  return GestureDetector(
    onTap: () {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DrugPage(drug,dcModel,ocModel),
        ),
      );
    },
    child:Card(
      color: Colors.white,
      child:ListTile(
        leading: FadeInImage(
          image: NetworkImage(
              drug.image),
          fit: BoxFit.contain,
          placeholder: AssetImage('assets/images/default.jpg'),
        ),
        title: Text(
          drug.name,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18
          ),
        ),
        subtitle: Text(
          drug.contain,
          style: TextStyle(
            fontSize: 14,
            color: Colors.black54,
          ),
        ),
        trailing: Text("price : ${priceGet(drug.price).toString()}",style: TextStyle(color: Colors.red),),
      )
    )
  );
}
