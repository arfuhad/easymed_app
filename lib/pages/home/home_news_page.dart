import 'package:flutter/material.dart';
import 'package:shopeasymeds/pages/home/elements/home_title_view.dart';

class HomeNewsPage extends StatefulWidget {
  // final Models model;this.model

  HomeNewsPage();

  @override
  State<StatefulWidget> createState() => _HomeNewsPage();
}

class _HomeNewsPage extends State<HomeNewsPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return newsPage();
  }

  Widget newsPage(){
    return Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(top: 35),
        color:Colors.teal[50],
        child:Column(
          children: <Widget>[
            titleView(first: "Latest ", last: "Update"),
            Container(
              height: MediaQuery.of(context).size.height - 144,
              child:ListView(
                //mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  // Container(
                  //   //padding: EdgeInsets.only(left:25),
                  //   height: 150,
                  //   child:ImageCarousel(),
                  // ),
                  SizedBox(height: 5,),
                  newsContainer(),
                  SizedBox(height: 5,),
                  newsContainer(),
                  SizedBox(height: 5,),
                  newsContainer(),
                ],
              )
            )
          ],
        )
    );
  }

  Widget newsContainer (){
    return Container(
      height: 200,
      child: Stack(
        children: <Widget>[
          Positioned(
            top:25,
            child:Container(
              width: MediaQuery.of(context).size.width,
              height: 150,
              child:Card(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(right: 15,top:10),
                      margin: EdgeInsets.only(left: MediaQuery.of(context).size.width/2.5),
                      width: MediaQuery.of(context).size.width/1.8,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "HeadingHeadingHeadingHeading",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20
                            ),
                          ),
                          Text(
                            "DetailsHeadingHeadingHeadingHeadingHeadingHeadingHeadingHeading",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14
                            ),
                          )
                        ],
                      )
                    ),
                  ]
                ),
              )
            )
          ),
          Positioned(
            left: 25,
            child: Image(
              //height: 90,
              image: AssetImage('assets/images/default.jpg'),
              width: MediaQuery.of(context).size.width/2.8,
            ),
          ),
          Positioned(
            bottom: 55,
            left: 35,
            child: Container(
              margin: EdgeInsets.only(top:50),
              child: Column(
                children: <Widget>[
                  Text(
                    "12/08/2019",
                    style: TextStyle(color: Colors.redAccent),
                  ),
                  SizedBox(height: 6,),
                  Text(
                    "View more",
                    style: TextStyle(color: Colors.teal),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}