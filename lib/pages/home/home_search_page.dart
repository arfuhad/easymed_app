import 'dart:async';

import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shopeasymeds/backend/backend_api_response.dart';
import 'package:shopeasymeds/controllers/controller_order.dart';
import 'package:shopeasymeds/pages/home/elements/home_title_view.dart';
import 'package:shopeasymeds/controllers/controller_drug.dart';
import 'package:shopeasymeds/pages/home/elements/search_list_view.dart';

class HomeSearchPage extends StatefulWidget{
  DrugControl dcModel;
  OrderControl ocModel;

  HomeSearchPage(this.dcModel,this.ocModel);

  @override
  State<StatefulWidget> createState() => _HomeSearchPage();

}

class _HomeSearchPage extends State<HomeSearchPage> {

  TextEditingController controller = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return searchPage();
  }

  Widget searchPage (){
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(top: 35),
      color:Colors.teal[50],
      child:ScopedModel<DrugControl>(
        model: DrugControl(value: false),
        child: ScopedModelDescendant<DrugControl>(
          builder: (BuildContext context, Widget child, DrugControl model){

            Timer(Duration(seconds: 5),(){

            });

            Widget content = Container();
            if(model.searchDrug.status == Status.COMPLETED)
            {
              content = drugListViewer(context,model.searchDrug.data,widget.dcModel,widget.ocModel);
            }
            if(model.searchDrug.status == Status.LOADING)
            {
              content = Center(
                  child:Container(
                    child: CircularProgressIndicator(),
                  )
                );
            }
            if(model.searchDrug.status == Status.ERROR)
            {
              content = Center(
                child: Container(
                  child: Text(model.searchDrug.message),
                ),
              );
            }
            return Column(
              children: <Widget>[
                titleView(first: "Sea", last: "rch"),
                Container(
                  margin: EdgeInsets.only(left:20,right: 20),
                  child: Card(
                    elevation: 15,
                    child: Container(
                      padding: EdgeInsets.only(left:15,right:15,bottom: 1),
                      child:TextField(
                        controller: controller,
                        cursorColor: Colors.red,
                        keyboardType: TextInputType.text,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                        decoration: InputDecoration(
                          hintText: "Search Here",
                          hintStyle: TextStyle(fontWeight: FontWeight.bold)
                        ),
                        onChanged: (text){
                          print("on change");
                          model.fetchSearch(qurey: controller.text.toLowerCase());
                        },
                        onSubmitted: (text){
                          print("on submit");
                          model.fetchSearch(qurey: controller.text.toLowerCase());
                        },
                      ),
                    ), 
                  ),
                ),
                //searchTextBox(model.fetchSearch),
                SizedBox(height: 2,),
                content
              ],
            );
          },
        ),
      ),
    );
  }
}