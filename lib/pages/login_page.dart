import 'package:flutter/material.dart';
// import 'package:scoped_model/scoped_model.dart';

// import '../backend/models.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _formData = {'password': null, 'mobile': null};

  @override
  Widget build(BuildContext context) {
    return LoginPage(context);
  }

  Widget mobileTextField() {
    return Container(
      margin: EdgeInsets.only(left:15,right:15),
      padding: EdgeInsets.only(left: 25.0, right: 25.0),
      child: TextFormField(
        keyboardType: TextInputType.number,
        textAlign: TextAlign.left,
        decoration: InputDecoration(
          fillColor: Colors.white,
          labelText: 'Enter your Mobile No.',
          labelStyle: TextStyle(color: Colors.red, fontSize: 16.0,fontWeight: FontWeight.bold),
          hintStyle: TextStyle(color: Colors.grey),
          hintText: '01XXXXXXXXX',
        ),
        style: TextStyle(
          color: Colors.white
        ),
        validator: (String value) {
          if (value.isEmpty || !RegExp(r"[0][1-9][0-9]{9}").hasMatch(value)) {
            return 'Please enter a valid mobile number';
          }
        },
        onSaved: (String value) {
          _formData['mobile'] = value;
        },
      ),
    );
  }

  Widget passTextField() {
    return Container(
      margin: EdgeInsets.only(left:15,right:15),
      padding: EdgeInsets.only(left: 25.0, right: 25.0),
      child: TextFormField(
        keyboardType: TextInputType.text,
        obscureText: true,
        textAlign: TextAlign.left,
        decoration: InputDecoration(
          labelText: 'Enter your Password',
          labelStyle: TextStyle(color: Colors.red, fontSize: 16.0,fontWeight: FontWeight.bold),
          hintStyle: TextStyle(color: Colors.grey),
          hintText: 'XXXXXXXXX',
        ),
        style: TextStyle(
          color: Colors.white
        ),
        validator: (String value) {
          if (value.isEmpty || value.length < 6) {
            return 'Please enter a atleast 6 character password';
          }
        },
        onSaved: (String value) {
          _formData['password'] = value;
        },
      ),
    );
  }

  Future _submitForm(Function authenticate) async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
    Map<String, dynamic> value;
    value = await authenticate(
        _formData['mobile'].toString(), _formData['password'].toString());
    if (value['success']) {
      Navigator.pushReplacementNamed(context, '/home');
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('An Error Occurred!'),
            content: Text(value['message']),
            actions: <Widget>[
              FlatButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    }
  }

  Widget LoginPage(BuildContext context) {
    return Scaffold(
      body: Stack(fit: StackFit.expand, children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.teal[50],
          ),
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 55.0,
              ),
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 18),
                      child: Image.asset(
                        'assets/icons/easymeds1.png',
                        height: 180,
                      )
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Text(
                      'Shop',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 32.0,
                        fontWeight: FontWeight.w800,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      'EasyMeds',
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: 32.0,
                        fontWeight: FontWeight.w800,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 40.0,
              ),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    mobileTextField(),
                    SizedBox(
                      height: 10.0,
                    ),
                    passTextField(),
                    SizedBox(
                      height: 20.0,
                    ),
                    Container(
                      margin: EdgeInsets.only(left:45,right:45),
                      padding: EdgeInsets.all(10.0),
                      child: 
                      // ScopedModelDescendant<Models>(builder:
                      //     (BuildContext context, Widget child, Models model) {
                      //   return model.loading
                      //       ? CircularProgressIndicator(
                      //         valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
                      //       )
                      //       : 
                            FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12.0)),
                                color: Colors.teal,
                                onPressed: () 
                                    {Navigator.pushReplacementNamed(context, '/home');},//_submitForm(model.authenticate),
                                child: Container(
                                  padding: const EdgeInsets.symmetric(
                                    vertical: 20.0,
                                    horizontal: 20.0,
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          "LOGIN",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )//;
                      //}),
                    ),
                    SizedBox(
                      height: 25.0,
                    ),
                    Container(
                        padding: EdgeInsets.all(10.0),
                        margin: EdgeInsets.only(left: 90.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Don't have account?",
                              style: TextStyle(
                                color: Colors.teal,
                                fontSize: 14.0,
                              ),
                            ),
                            FlatButton(
                              onPressed: () => Navigator.pushReplacementNamed(
                                  context, '/signup'),
                              child: Text(
                                "Sign Up",
                                style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                          ],
                        )),
                  ],
                ),
              )
            ],
          ),
        )
      ]),
    );
  }
}
