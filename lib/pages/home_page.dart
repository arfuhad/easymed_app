import 'package:flutter/material.dart';
import 'package:shopeasymeds/controllers/controller_drug.dart';
import 'package:shopeasymeds/controllers/controller_order.dart';
import 'package:shopeasymeds/models/order.dart';

import 'package:shopeasymeds/pages/home/home_product_page.dart';
import 'package:shopeasymeds/pages/home/home_search_page.dart';
import 'package:shopeasymeds/pages/home/home_news_page.dart';
import 'package:shopeasymeds/pages/home/home_cart_page.dart';
import 'package:shopeasymeds/pages/home/home_profile_page.dart';

class HomePage extends StatefulWidget {
  // final Models model;this.model
  final DrugControl dcModel;
  final OrderControl ocModel;

  HomePage(this.dcModel,this.ocModel);

  @override
  State<StatefulWidget> createState() => _HomePage();
}

class _HomePage extends State<HomePage> {
  int _selectedTab = 0;
  static DrugControl dcModel; //= widget.dcModel;
  static OrderControl ocModel;

  _HomePage(){
    dcModel = DrugControl(value: true);
    ocModel = OrderControl();
  }

  final _pageOption = [
      HomeDrugPage(dcModel,ocModel),
      HomeNewsPage(),
      HomeSearchPage(dcModel,ocModel),
      HomeCartPage(ocModel),
      HomeProfilePage()
    ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset : false,
      body: _pageOption[_selectedTab],
      bottomNavigationBar: BottomNavigationBar(
        elevation: 25,
        currentIndex: _selectedTab,
        onTap: (int index){
          setState(() {
            _selectedTab = index;
          });
        },
        items: navigationBarItems(),
      ),
    );
  }

  navigationBarItems (){
    List<BottomNavigationBarItem> value = [
      BottomNavigationBarItem(
        icon: Icon(
          Icons.list,
          color: _selectedTab == 0 ? Colors.teal : Colors.grey ,
        ),
        title: Container(
          child: Text("Products",style: TextStyle(color: Colors.black,fontWeight: FontWeight.w500),),
        )
      ),
      BottomNavigationBarItem(
        icon: Icon(
          Icons.dashboard,
          color: _selectedTab == 1 ? Colors.teal : Colors.grey ,
        ),
        title: Container(
          child: Text("News",style: TextStyle(color: Colors.black,fontWeight: FontWeight.w500),),
        )
      ),
      BottomNavigationBarItem(
        icon: Icon(
          Icons.search,
          color: _selectedTab == 2 ? Colors.teal : Colors.grey ,
        ),
        title: Container(
          child: Text("Search",style: TextStyle(color: Colors.black,fontWeight: FontWeight.w500),),
        )
      ),
      BottomNavigationBarItem(
        icon: Icon(
          Icons.shopping_cart,
          color: _selectedTab == 3 ? Colors.teal : Colors.grey ,
        ),
        title: Container(
          child: Text("Cart",style: TextStyle(color: Colors.black,fontWeight: FontWeight.w500),)
        )
      ),
      BottomNavigationBarItem(
        icon: Icon(
          Icons.account_box,
          color: _selectedTab == 4 ? Colors.teal : Colors.grey ,
        ),
        title: Container(
          child: Text("Profile",style: TextStyle(color: Colors.black,fontWeight: FontWeight.w500),),
        )
      ),
    ];
    return value;
  }
}