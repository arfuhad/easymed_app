import 'package:flutter/material.dart';
import 'dart:async';
// import 'package:shared_preferences/shared_preferences.dart';
// import '../backend/models.dart';

class SplashScreen extends StatefulWidget {
  //final Models _model;this._model
  SplashScreen();
  @override
  State<StatefulWidget> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin {
  // Future<String> loginCheck() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   return prefs.getBool('setuser').toString();
  // }

  Animation _animationBegin;
  AnimationController _animationControllerBegin;

  @override
  void initState() {
    _animationControllerBegin = AnimationController(duration: Duration(milliseconds: 2000),vsync: this);

    _animationBegin = Tween(begin: 0.0, end:100).animate(_animationControllerBegin)
      ..addListener((){
        setState((){
          
        });
      });

    _animationControllerBegin.forward();
    // Timer(Duration(seconds: 4), () async {
    //   // if (await loginCheck() == 'true') {
    //   //   if (await widget._model.setUserFromPref()) {
    //   //     Navigator.pushReplacementNamed(context, '/home');
    //   //   } else {
    //   //     print('error in data Fetch');
    //   //   }
    //   // } else {
    //     Navigator.pushReplacementNamed(context, '/second');
    //   //}
    // });
    // Timer(Duration(seconds: 3), (){_animationControllerBegin.reverse();});

    
    
    Timer(Duration(seconds: 3),(){
      
      Navigator.pushReplacementNamed(context, '/second');
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Colors.teal[50],
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: double.parse(_animationBegin.value.toString())*2,
                        margin: EdgeInsets.only(left: 35),
                        child: Image.asset(
                          'assets/icons/easymeds1.png',
                        )
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      Opacity(
                        opacity: double.parse(_animationBegin.value.toString())/100,
                        child:Column(
                          children: <Widget>[
                            Text(
                              'Shop',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 35.0,
                                fontWeight: FontWeight.w800,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            Text(
                              'EasyMeds',
                              style: TextStyle(
                                color: Colors.red,
                                fontSize: 35.0,
                                fontWeight: FontWeight.w800,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ]
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _animationControllerBegin.dispose();
    super.dispose();
  }
}
