import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shopeasymeds/backend/backend_api_response.dart';
import 'package:shopeasymeds/controllers/controller_drug.dart';
import 'package:shopeasymeds/controllers/controller_order.dart';
import 'package:shopeasymeds/models/drug.dart';
import 'package:shopeasymeds/pages/home/elements/product_hori_view.dart';

class DrugPage extends StatefulWidget{
  Drug drug;
  DrugControl dcModel;
  OrderControl ocModel;

  DrugPage(this.drug,this.dcModel,this.ocModel);

  @override
  State<StatefulWidget> createState() => _DrugPage();

}

class _DrugPage extends State<DrugPage> {

  double imageHeight = 350;

  @override
  Widget build(BuildContext context) {
    return drugView();
  }

  Widget drugView(){
    return Scaffold(
      backgroundColor: Colors.teal[50],
      body: Stack(
          children: <Widget>[
            imageViewer(),
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: ListView(
                padding: EdgeInsets.only(top: imageHeight-80),
                children: <Widget>[
                  detailViewer(),
                ],
              ),
            ),
            topNavigation(),
          ],
        ),

    );
  }

  Widget topNavigation(){
    return SafeArea(
      child: Row(
        children: <Widget>[
          FlatButton(
            onPressed: (){},
            child: Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                color: Colors.black45,
                shape: BoxShape.circle,
              ),
              child: Icon(Icons.arrow_back_ios,color: Colors.white,),
            ),
          ),
          Spacer(),
          FlatButton(
            onPressed: (){},
            child: Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                color: Colors.black45,
                shape: BoxShape.circle,
              ),
              child: Icon(Icons.home,color: Colors.white,),
            ),
          )
        ],
      ),
    );
  }

  Widget imageViewer(){
    return Positioned(
      top: 0,
      left: 0,
      child: Container(
        height: imageHeight + 20,
        width: MediaQuery.of(context).size.width,
        child: FadeInImage(
          image: NetworkImage(widget.drug.image),
          fit: BoxFit.fitHeight,
          placeholder: AssetImage('assets/images/default.jpg'),
        ),
      ),
    );
  }

  Widget detailViewer(){
    return Stack(
      children: <Widget>[
        drugDetail(),
        Positioned(
          top: 0,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 80,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                colors: [
                  Colors.grey.withOpacity(0.4),
                  Colors.grey.withOpacity(0.0),
                ],
                stops: [
                  0.0,
                  1.0
                ]
              )
            ),
            child: Column(
              children: <Widget>[
                Text(
                  widget.drug.name,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 25,
                    fontWeight: FontWeight.bold
                  ),
                ),
                Text(
                  widget.drug.manufacturer,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          top: 55,
          left: MediaQuery.of(context).size.width/4,
          child: Container(
            width: MediaQuery.of(context).size.width/2,
            child:ScopedModel<OrderControl>(
              model: widget.ocModel,
              child: ScopedModelDescendant<OrderControl>(
                builder: (BuildContext context, Widget child, OrderControl model){
                  String btnText = "Add to Cart";
                  Color btnColor = Colors.teal;
                  return RaisedButton(
                    onPressed: (){
                      if(model.isPlaced(widget.drug)){
                        btnText = "Remove from Cart";
                        btnColor = Colors.red;
                        model.deleteCart(widget.drug.id);
                        setState(() {
                        });
                      }
                      else{
                        model.addCart(widget.drug);
                        btnText = "Added to Cart";
                        btnColor = Colors.teal;
                        setState(() {
                        });
                      }
                    },
                    color: btnColor,
                    child: Text(
                      btnText,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget drugDetail(){
    return Container(
      margin: EdgeInsets.only(top: 80),
      height: MediaQuery.of(context).size.height -80,
      decoration: BoxDecoration(
        color: Colors.teal[50],
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25),
          topRight: Radius.circular(25)
        )
      ),
      child: Container(
        margin: EdgeInsets.only(top: 35),
        padding: EdgeInsets.symmetric(horizontal: 10),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Card(
              child: Container(
                height: 100,
                child: ListTile(
                  title: Container(
                    margin: EdgeInsets.symmetric(vertical: 8),
                    child: Text(
                      widget.drug.contain,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18
                      ),
                    ),
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(widget.drug.form),
                    ],
                  ),
                  trailing: Text("Price ${priceGet(widget.drug.price).toString()}"),
                ),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            ScopedModel<DrugControl>(
              model: DrugControl(value: false,dclass: widget.drug.contain),
              child: ScopedModelDescendant<DrugControl>(
                builder: (BuildContext context, Widget child, DrugControl model){
                  Widget content = Container();
                  if(model.similarDrug.status ==  Status.COMPLETED){
                    content = productContainer("Similar Drugs",context,model.similarDrug,false,widget.ocModel,model,fontSize: 25);
                  }
                  if(model.similarDrug.status ==  Status.ERROR){
                    content = productContainer("Similar Drugs",context,model.similarDrug,true,widget.ocModel,model,fontSize: 25);
                  }
                  if(model.similarDrug.status == Status.LOADING){
                    content = Center(child: Container(child: CircularProgressIndicator(),),);
                  }
                  return content;
                },
              ),
            ),
            // productContainer("Similar Drugs",context,)
            // Text(
            //   "Similar Drugs",
            //   style: TextStyle(
            //     fontWeight: FontWeight.bold,
            //     fontSize: 25
            //   ),
            // ),
            // SizedBox(
            //   height: 5,
            // ),
          ],
        )
      )
    );
  }


  double priceGet(String value){
    List<String> list = value.split(" ");
    print(list.toString());
    var x = double.parse(list[list.length - 2]);
    // if()
    if(! (x is double)){
      x = double.parse(list[list.length - 1]);
    }
    print(x.toString());
    return x;
  }
}
