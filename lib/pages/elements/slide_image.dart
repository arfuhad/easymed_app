import 'package:flutter/material.dart';

class ImageCarousel extends StatefulWidget {
  _ImageCarouselState createState() => _ImageCarouselState();
}

class _ImageCarouselState extends State<ImageCarousel> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        Card(
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: FadeInImage(
            image: NetworkImage(
                'http://www.kconnect.eu/sites/default/files/styles/top_teaser/public/images/health_on_the_net.jpg?itok=xdIRhd4o'),
            fit: BoxFit.cover,
            placeholder: AssetImage('assets/images/default.jpg'),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 5,
          margin: EdgeInsets.all(10),
        ),
        Card(
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: FadeInImage(
            image: NetworkImage(
                'http://africanhealthsciences.org/wp-content/uploads/2017/10/75.jpg'),
            fit: BoxFit.cover,
            placeholder: AssetImage('assets/images/default.jpg'),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 5,
          margin: EdgeInsets.all(10),
        ),
        Card(
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: FadeInImage(
            image:
                NetworkImage('https://health.punjab.gov.pk/img/slider/22.jpg'),
            fit: BoxFit.cover,
            placeholder: AssetImage('assets/images/default.jpg'),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 5,
          margin: EdgeInsets.all(10),
        ),
      ],
    );
  }
}
