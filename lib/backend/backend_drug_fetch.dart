import 'package:shopeasymeds/backend/backend_config.dart';

class DrugFetch extends BackendConfig {

  String _errorMessage = "Sorry, data can't be fetch at this moment, try again or check internet connection";
  
  Future<String> fetchDrug([String page, String pageSize]) async {
    String link = '/drug';
    if(page != null || pageSize != null){
      link = '/drug?pageNumber=${page}&pageSize=${pageSize}';
    }
    dynamic res = await get(link);
    return res;
  }

  Future<String> fetchBest() async {
    String link = '/drug/bestSelling';
    dynamic res = await get(link);
    return res;
  }

  Future<String> fetchRecommended(String id) async {
    String link = '/drug/recommended?customerId=${id}';
    dynamic res = await get(link);
    return res;
  }

  Future<String> fetchSimilar(String value) async {
    String link = '/drug/sameDrug?qurey=${value}';
    dynamic res = await get(link);
    return res;
  }

  Future<String> fetchSearch({String qurey = ''}) async {
    String link = '/drug?searchTerm=${qurey}';
    dynamic res = await get(link);
    return res;
  }

}
