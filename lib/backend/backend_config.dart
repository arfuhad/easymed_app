import 'package:http/http.dart' as http;
import 'package:shopeasymeds/backend/backend_exeptions.dart';

class BackendConfig{
  final String _baseUrl =  "http://shopeasymeds.com";//"http://192.168.1.113";
  final String _port = ":8080/api";

  Future<String> get(String url) async {
    var responseJson;
    try {
      final response = await http.get(_baseUrl + _port + url);
      responseJson = _returnResponse(response);
    } catch (SocketException) {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  String _returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = response.body.toString();
        //print(responseJson);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}