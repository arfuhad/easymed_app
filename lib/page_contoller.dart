import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shopeasymeds/controllers/controller_drug.dart';
import 'package:shopeasymeds/controllers/controller_order.dart';
//import 'package:scoped_model/scoped_model.dart';
import 'dart:async';
//import 'package:firebase_messaging/firebase_messaging.dart';
//import 'package:path_provider/path_provider.dart';

import 'package:shopeasymeds/pages/splash_page.dart';
import 'package:shopeasymeds/pages/choose_page.dart';
import 'package:shopeasymeds/pages/login_page.dart';
import 'package:shopeasymeds/pages/signup_page.dart';
import 'package:shopeasymeds/pages/home_page.dart';
// import './pages/cart_page.dart';
// import './pages/profile_page.dart';
// import './pages/checkout_page.dart';
// import './pages/order_page.dart';
// import './pages/prescription_page.dart';
// import './pages/drugList_page.dart';
// import './backend/models.dart';

class PageNaviController extends StatefulWidget {
  // final Models _model;this._model

  PageNaviController();

  @override
  State<StatefulWidget> createState() => _PageNaviController();
}

class _PageNaviController extends State<PageNaviController> {

  //final FirebaseMessaging _messaging = FirebaseMessaging();
  String token = "";

  DrugControl dcModel;
  OrderControl ocModel;

  @override
  void initState() {
    super.initState();
    // _messaging.getToken().then((token){
    //   print(token);
    //   writeCounter(token);
    // });
    dcModel = DrugControl(value: true);
    ocModel = OrderControl();
  }

  // Future<String> get _localPath async {
  //   final directory = await getApplicationDocumentsDirectory();
  //   return directory.path;
  // }

  // Future<File> get _localFile async {
  //   final path = await _localPath;
  //   //print(_localPath);
  //   return File('$path/counter.txt');
  // }

  // Future<File> writeCounter(String counter) async {
  //   final file = await _localFile;
  //   print(file.path);
  //   // Write the file
  //   return file.writeAsString('$counter');
  // }

  @override
  Widget build(BuildContext context) {
    return 
    // ScopedModel<Models>(
    //   model: widget._model,
    //   child: 
      MaterialApp(
        title: 'ShopEasyMeds',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: Colors.black,
          accentColor: Colors.yellow[800],
          fontFamily: 'Montserrat'
        ),
        // Start the app with the "/" named route. In our case, the app will start
        // on the FirstScreen Widget
        initialRoute: '/',
        routes: {
          '/': (context) => SplashScreen(),
          '/second': (context) => ChooseScreen(),
          '/login': (context) => LoginPage(),
          '/signup': (context) => SignUpPage(),
          '/home': (context) => HomePage(dcModel,ocModel),
          // '/cart': (context) => CartPage(),
          // '/profile': (context) => ProfilePage(widget._model),
          // '/checkout': (context) => CheckoutPage(widget._model),
          // '/order': (context) => OrderPage(widget._model),
          // '/prescription': (context) => PrescriptionPage(widget._model),
          // '/drugList': (context) => DrugListPage(widget._model)
        },
      //),
    );
  }
}
