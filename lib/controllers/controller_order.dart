import 'dart:convert';
import 'dart:math';

import 'package:scoped_model/scoped_model.dart';
import 'package:shopeasymeds/backend/backend_api_response.dart';
import 'package:shopeasymeds/backend/backend_order_fetch.dart';
import 'package:shopeasymeds/models/drug.dart';

class OrderControl extends Model{

  double _amount = 0;
  bool _isLoading = false;
  OrderFetch _orderFetch = OrderFetch();
  // DrugApiResponse _fetchedDrug = DrugApiResponse();
  List<Drug> _cartDrug = [];
  // DrugApiResponse _orderDrug = DrugApiResponse();
  
  // DrugApiResponse<List<Drug>> get fetchedDrug => _fetchedDrug;
  List<Drug> get cartDrug => List.from(_cartDrug);
  // DrugApiResponse<List<Drug>> get orderDrug => _orderDrug;
  
  bool get loading {
    return _isLoading;
  }

  toggleLoading(){
    _isLoading = ! _isLoading;
  }

  Future<bool> tryAgain() async{
    if(loading){
      await Future.delayed(Duration(seconds: 2));
      tryAgain();
    }
    return !loading;
  }


  addCart(Drug drug) async {
    if(!await tryAgain()){
      return;
    }
    toggleLoading();
    notifyListeners();

    print(_cartDrug.toString());
    double price = priceGet(drug.price);
    if (_cartDrug.length == 0) {
      drug.quantity++;
      _cartDrug.add(drug);
      incAmount(price);
      print("just added " + drug.name);
    } else {
      if (_cartDrug.contains(drug)) {
        int x = _cartDrug.indexOf(drug);
        _cartDrug[x].quantity++;
        incAmount(price);
        print("just inc " + drug.name);
      } else {
        drug.quantity++;
        _cartDrug.add(drug);
        incAmount(price);
        print("just added " + drug.name);
      }
    }
    toggleLoading();
    notifyListeners();
  }

  deleteCart(int index) async {
    if(!await tryAgain()){
      return;
    }
    toggleLoading();
    notifyListeners();
    if (_cartDrug.length != 0) {
      for (var i = 0; i < _cartDrug.length; i++) {
        if (_cartDrug[i].id == index) {
          double price = priceGet(_cartDrug[i].price);
          if (_cartDrug[i].quantity > 1) {
            decAmount(price);
            if (_cartDrug[i].quantity > 0) {
              _cartDrug[i].quantity--;
            }
            print("just drc " + _cartDrug[i].name);
          } else {
            decAmount(price);
            if (_cartDrug[i].quantity > 0) {
              _cartDrug[i].quantity--;
            }
            print("just deleted " + _cartDrug[i].name);
            _cartDrug.removeAt(i);
          }
        }
      }
    } else {
      _amount = 0;
    }
    toggleLoading();
    notifyListeners();

  }

  // fetchDrug() async {
  //   _fetchedDrug = ApiResponse.loading("Fetching Products");
  //   notifyListeners();
  //   try{
  //     List<Drug> _fetched = drugFromJson( await _orderFetch.fetchDrug());
  //     List<Drug> _blank = [];
  //     _blank = _fetchedDrug.data;
  //     if(_blank == null || _blank.isEmpty){
  //       _fetchedDrug = ApiResponse.completed(_fetched);
  //       // _tempDrug = _fetched;
  //     }else{
  //       _blank = _fetchedDrug.data;
  //       for (int i = 0; i < _fetched.length; i++) {
  //         if (!_blank.contains(_fetched[i])) {
  //           _blank.add(_fetched[i]);
  //         }
  //       }
  //       _fetchedDrug = ApiResponse.completed(_blank);
  //       // _tempDrug = _blank;
  //     }
  //     notifyListeners();
  //   } catch (e){
  //     _fetchedDrug = ApiResponse.error(e.toString());
  //     notifyListeners();
  //   }
  // }

  List<Drug> drugFromJson(String str) {
    //print(str);
    final jsonData = json.decode(str);
    //print(str);
    return new List<Drug>.from(jsonData.map((x) => Drug.fromJson(x)));
  }

  String drugToJson(List<Drug> data) {
    final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
    return json.encode(dyn);
  }

  double get amount {
    return _amount;
  }

  void incAmount(double x) {
    _amount = _amount + x;
    _amount = dp(_amount, 2);
    print("amount " + _amount.toString());
  }

  void decAmount(double x) {
    if (_amount > 0) {
      _amount = _amount - x;
      _amount = dp(_amount, 2);
    } else {
      _amount = 0;
    }
    print("amount " + _amount.toString());
  }

  double dp(double val, double places) {
    double mod = pow(10.0, places);
    return ((val * mod).round().toDouble() / mod);
  }

  double priceGet(String value){
    List<String> list = value.split(" ");
    return double.parse(list[list.length - 2]);
  }

  bool isPlaced(drug) {
    try{
      if(_cartDrug == null){
        return false;
      }else{
        if(!_cartDrug.contains(drug)){
          return false;
        }
      }
      return true;
    }catch(e){
      print(e.toString());
      return false;
    }
  }
}