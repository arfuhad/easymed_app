import 'dart:convert';

import 'package:scoped_model/scoped_model.dart';
import 'package:shopeasymeds/models/drug.dart';
import 'package:shopeasymeds/backend/backend_drug_fetch.dart';
import 'package:shopeasymeds/backend/backend_api_response.dart';

class DrugControl extends Model {

  List<Drug> _tempDrug = [];
  // List<int> _recommendedDrug = [];
  // List<int> _bestSellingDrug = [];
  // List<int> _similarDrug = [];
  // List<int> _searchDrug = [];
  // List<int> _recentSearchDrug = [];
  // bool searchDone = false;

  DrugFetch _drugFetch;
  ApiResponse<List<Drug>> _fetchedDrug;
  ApiResponse<List<Drug>> _recommendedDrug;
  ApiResponse<List<Drug>> _bestSellingDrug;
  ApiResponse<List<Drug>> _similarDrug;
  ApiResponse<List<Drug>> _searchDrug;
  ApiResponse<List<Drug>> _recentSearchDrug;

  ApiResponse<List<Drug>> get fetchedDrug => _fetchedDrug;
  ApiResponse<List<Drug>> get recommendedDrug => _recommendedDrug;
  ApiResponse<List<Drug>> get bestSellingDrug => _bestSellingDrug;
  ApiResponse<List<Drug>> get similarDrug => _similarDrug;
  ApiResponse<List<Drug>> get searchDrug => _searchDrug;
  ApiResponse<List<Drug>> get recentSearchDrug => _recentSearchDrug;


  DrugControl({bool value = false, String dclass = ""}){
    _drugFetch = DrugFetch();

    _fetchedDrug = ApiResponse();
    _recommendedDrug = ApiResponse();
    _bestSellingDrug = ApiResponse();
    _similarDrug = ApiResponse();
    _searchDrug = ApiResponse();
    _recentSearchDrug = ApiResponse();

    if(value){
      fetchDrug();
      fetchBest();
      fetchRecommended();
    }else{
      fetchSearch();
      fetchSimilar(dclass);
    }
    
  }

  fetchDrug() async {
    _fetchedDrug = ApiResponse.loading("Fetching Products");
    notifyListeners();
    try{
      List<Drug> _fetched = drugFromJson( await _drugFetch.fetchDrug());
      List<Drug> _blank = [];
      _blank = _fetchedDrug.data;
      if(_blank == null || _blank.isEmpty){
        _fetchedDrug = ApiResponse.completed(_fetched);
        _tempDrug = _fetched;
      }else{
        _blank = _fetchedDrug.data;
        for (int i = 0; i < _fetched.length; i++) {
          if (!_blank.contains(_fetched[i])) {
            _blank.add(_fetched[i]);
          }
        }
        _fetchedDrug = ApiResponse.completed(_blank);
        _tempDrug = _blank;
      }
      notifyListeners();
    } catch (e){
      _fetchedDrug = ApiResponse.error(e.toString());
      notifyListeners();
    }
  }

  fetchBest() async {
    _bestSellingDrug = ApiResponse.loading("Fetching Products");
    notifyListeners();
    try{
      List<Drug> _fetched = drugFromJson( await _drugFetch.fetchBest());
      List<Drug> _blank = [];
      _blank = _bestSellingDrug.data;
      if(_blank == null || _blank.isEmpty){
        _bestSellingDrug = ApiResponse.completed(_fetched);
      }else{
        _blank = _bestSellingDrug.data;
        for (int i = 0; i < _fetched.length; i++) {
          if (!_blank.contains(_fetched[i])) {
            _blank.add(_fetched[i]);
          }
        }
        _bestSellingDrug = ApiResponse.completed(_blank);
      }
      notifyListeners();
    } catch (e){
      _bestSellingDrug = ApiResponse.error(e.toString());
      notifyListeners();
    }
  }

  fetchRecommended({String id}) async {
    _recommendedDrug = ApiResponse.loading("Fetching Products");
    notifyListeners();
    try{
      List<Drug> _fetched = drugFromJson( await _drugFetch.fetchRecommended("24"));
      List<Drug> _blank = [];
      _blank = _recommendedDrug.data;
      if(_blank == null || _blank.isEmpty){
        _recommendedDrug = ApiResponse.completed(_fetched);
      }else{
        _blank = _recommendedDrug.data;
        for (int i = 0; i < _fetched.length; i++) {
          if (!_blank.contains(_fetched[i])) {
            _blank.add(_fetched[i]);
          }
        }
        _recommendedDrug = ApiResponse.completed(_blank);
      }
      notifyListeners();
    } catch (e){
      _recommendedDrug = ApiResponse.error(e.toString());
      notifyListeners();
    }
  }

  fetchSimilar(String value) async {
    _similarDrug = ApiResponse.loading("Fetching Products");
    notifyListeners();
    try{
      List<Drug> _fetched = drugFromJson(await _drugFetch.fetchSimilar(value));
      List<Drug> _blank = [];
      _blank = _similarDrug.data;
      if(_blank == null || _blank.isEmpty){
        _similarDrug = ApiResponse.completed(_fetched);
      }else{
        _blank = _similarDrug.data;
        for (int i = 0; i < _fetched.length; i++) {
          if (!_blank.contains(_fetched[i])) {
            _blank.add(_fetched[i]);
          }
        }
        _similarDrug = ApiResponse.completed(_blank);
      }
      notifyListeners();
    } catch (e) {
      _similarDrug = ApiResponse.error(e.toString());
      notifyListeners();
    }
  }

  fetchSearch({String qurey = ''}) async {
    print(qurey);
    _searchDrug = ApiResponse.loading("Fetching Products");
    notifyListeners();
    try{
      List<Drug> _blank = [];
      if(qurey == '' || qurey.isEmpty){
        if(recentSearchDrug.data == null){
          print("recent is empty");
          //print(_fetchedDrug.data.toString());
          if((fetchedDrug.data == null) & (_tempDrug != null)){
            _blank = _tempDrug;
          }else{
            fetchDrug();
            _blank = fetchedDrug.data;
          }
          print(_blank.toString());
        } else {
          print("recent is full");
          _blank = recentSearchDrug.data;
        }
        _searchDrug = ApiResponse.completed(_blank);
      }
      else{ 
        if(searchDrug.data != null){
          _searchDrug.data.clear();
        }
        print("in the fetch");
        List<Drug> _fetched = drugFromJson(await _drugFetch.fetchSearch(qurey: qurey));
        //_blank = _searchDrug.data;
        if(_fetched.isNotEmpty){
          _searchDrug = ApiResponse.completed(_fetched);
        }
        notifyListeners();
      }
    } catch (e) {
      _searchDrug = ApiResponse.error(e.toString());
      notifyListeners();
    }
  }

  List<Drug> drugFromJson(String str) {
    //print(str);
    final jsonData = json.decode(str);
    //print(str);
    return new List<Drug>.from(jsonData.map((x) => Drug.fromJson(x)));
  }

  String drugToJson(List<Drug> data) {
    final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
    return json.encode(dyn);
  }
}
