import 'dart:convert';

class Drug {
  int id;
  String name;
  String contain;
  String form;
  String price;
  String image;
  String manufacturer;
  int quantity;

  Drug({
    this.id,
    this.name,
    this.contain,
    this.form,
    this.price,
    this.image,
    this.manufacturer,
    this.quantity = 0,
  });

  factory Drug.fromJson(Map<String, dynamic> json) => new Drug(
    id: json["id"],
    name: json["name"],
    contain: json["contain"],
    form: json["form"],
    price: json["price"],
    image: json["image"],
    manufacturer: json["manufacturer"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "contain": contain,
    "form": form,
    "price": price,
    "image": image,
    "manufacturer": manufacturer,
    "quantity": quantity
  };

  Map<String, dynamic> toPlaceJson() => {
    "id": id,
    "quantity": quantity
  };
}
