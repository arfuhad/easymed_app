// To parse this JSON data, do
//
//     final order = orderFromJson(jsonString);

class Order {
  int id;
  DateTime datetime;
  double amount;
  int customerId;
  int riderId;
  int pharmaId;
  int statusId;
  int active;
  String orderNumber;
  String prescription;
  String address;
  String latitude;
  String longitude;
  DateTime delivered_on;
  int voucherId;
  String customerName;
  dynamic pharmaName;
  String status;
  List<Drugs> drugs;

  Order({
    this.id,
    this.datetime,
    this.amount,
    this.customerId,
    this.riderId,
    this.pharmaId,
    this.statusId,
    this.active,
    this.orderNumber,
    this.prescription,
    this.address,
    this.latitude,
    this.longitude,
    this.delivered_on,
    this.voucherId,
    this.customerName,
    this.pharmaName,
    this.status,
    this.drugs,
  });

  factory Order.fromJson(Map<String, dynamic> json) => new Order(
        id: json["id"],
        datetime: DateTime.parse(json["datetime"]),
        amount: json["amount"].toDouble(),
        customerId: json["customer_id"],
        riderId: json["rider_id"],
        pharmaId: json["pharma_id"],
        statusId: json["status_id"],
        active: json["active"],
        orderNumber: json["order_number"],
        prescription:
            json["prescription"] == null ? null : json["prescription"],
        address: json["address"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        delivered_on: json["delivered_on"] == null ? null : json["delivered_on"],
        voucherId: json["voucherId"] == null ? 0 : json["voucherId"],
        customerName: json["customerName"],
        pharmaName: json["pharmaName"],
        status: json["status"],
        drugs:
            new List<Drugs>.from(json["drugs"].map((x) => Drugs.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "datetime": datetime.toIso8601String(),
        "amount": amount,
        "customer_id": customerId,
        "rider_id": riderId,
        "pharma_id": pharmaId,
        "status_id": statusId,
        "active": active,
        "order_number": orderNumber,
        "prescription": prescription == null ? null : prescription,
        "address": address,
        "latitude": latitude,
        "longitude": longitude,
        "delivered_on": delivered_on,
        "voucherId": voucherId,
        "customerName": customerName,
        "pharmaName": pharmaName,
        "status": status,
        "drugs": new List<dynamic>.from(drugs.map((x) => x.toJson())),
      };
}

class Drugs {
  int orderId;
  int drugId;
  String name;
  String price;
  int quantity;

  Drugs({
    this.orderId,
    this.drugId,
    this.name,
    this.price,
    this.quantity,
  });

  factory Drugs.fromJson(Map<String, dynamic> json) => new Drugs(
        orderId: json["order_id"],
        drugId: json["drug_id"],
        name: json["name"],
        price: json["price"],
        quantity: json["quantity"],
      );

  Map<String, dynamic> toJson() => {
        "order_id": orderId,
        "drug_id": drugId,
        "name": name,
        "price": price,
        "quantity": quantity,
      };
}
