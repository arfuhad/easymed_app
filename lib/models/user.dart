class User {
  int id;
  String name;
  String mobile;
  String dob;
  String address;
  String latitude;
  String longitude;
  String email;
  int userCategory;
  String image;
  int iat;
  int exp;

  User({
    this.id,
    this.name,
    this.mobile,
    this.dob,
    this.address,
    this.latitude,
    this.longitude,
    this.email,
    this.userCategory,
    this.image,
    this.iat,
    this.exp,
  });

  factory User.fromJson(Map<String, dynamic> json) => new User(
        id: json["customerId"],
        name: json["name"],
        mobile: json["mobile"],
        dob: json["dob"],
        address: json["address"],
        latitude: json["lat"]==null?json["latitude"]:json["lat"],
        longitude: json["lng"]==null?json["longitude"]:json["longitude"],
        email: json["email"],
        userCategory: json["category"],
        image: json["image"],
        iat: json["iat"],
        exp: json["exp"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "mobile": mobile,
        "dob": dob,
        "address": address,
        "latitude": latitude,
        "longitude": longitude,
        "email": email,
        "userCategory": userCategory,
        "image": image,
        "iat": iat,
        "exp": exp,
      };
}
