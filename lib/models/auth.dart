class Auth {
    String token;

    Auth({
        this.token,
    });

    factory Auth.fromJson(Map<String, dynamic> json) => new Auth(
        token: json["token"]
    );

    Map<String, dynamic> toJson() => {
        "token": token
    };
}